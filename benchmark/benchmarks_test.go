package benchmarks

import (
	"testing"
)

type globber interface {
	Glob(string) bool
}

func benchmarkGlobMatching(b *testing.B, g globber) {
	for i := 0; i < b.N; i++ {
		if !g.Glob("aaaaabbbbbcccccdddddeeeeefffff") {
			b.Errorf("Globber did not match")
		}
	}
}

func benchmarkGlobNotMatching(b *testing.B, g globber) {
	for i := 0; i < b.N; i++ {
		if g.Glob("uuuuuvvvvvwwwwwxxxxxyyyyyzzzzz") {
			b.Errorf("Globber did match, but should not")
		}
	}
}
