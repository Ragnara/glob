package benchmarks

import (
	"bitbucket.org/ragnara/glob"
	"testing"
)

func BenchmarkStartGlobMatching_own(b *testing.B) {
	benchmarkGlobMatching(b, glob.New("*dddddeeeeefffff", glob.Default))
}

func BenchmarkMiddleGlobMatching_own(b *testing.B) {
	benchmarkGlobMatching(b, glob.New("*cccccddddd*", glob.Default))
}

func BenchmarkEndGlobMatching_own(b *testing.B) {
	benchmarkGlobMatching(b, glob.New("aaaaabbbbbccccc*", glob.Default))
}

func BenchmarkStartGlobNotMatching_own(b *testing.B) {
	benchmarkGlobNotMatching(b, glob.New("*dddddeeeeefffff", glob.Default))
}

func BenchmarkMiddleGlobNotMatching_own(b *testing.B) {
	benchmarkGlobNotMatching(b, glob.New("*cccccddddd*", glob.Default))
}

func BenchmarkEndGlobNotMatching_own(b *testing.B) {
	benchmarkGlobNotMatching(b, glob.New("aaaaabbbbbccccc*", glob.Default))
}
