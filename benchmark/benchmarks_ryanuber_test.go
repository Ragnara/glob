package benchmarks

import (
	ryanuber "github.com/ryanuber/go-glob"
	"testing"
)

type ryanuberGlobber struct {
	pattern string
}

func (r *ryanuberGlobber) Glob(s string) bool {
	return ryanuber.Glob(r.pattern, s)
}

func BenchmarkStartGlobMatching_ryanuber(b *testing.B) {
	benchmarkGlobMatching(b, &ryanuberGlobber{"*dddddeeeeefffff"})
}

func BenchmarkMiddleGlobMatching_ryanuber(b *testing.B) {
	benchmarkGlobMatching(b, &ryanuberGlobber{"*cccccddddd*"})
}

func BenchmarkEndGlobMatching_ryanuber(b *testing.B) {
	benchmarkGlobMatching(b, &ryanuberGlobber{"aaaaabbbbbccccc*"})
}

func BenchmarkStartGlobNotMatching_ryanuber(b *testing.B) {
	benchmarkGlobNotMatching(b, &ryanuberGlobber{"*dddddeeeeefffff"})
}

func BenchmarkMiddleGlobNotMatching_ryanuber(b *testing.B) {
	benchmarkGlobNotMatching(b, &ryanuberGlobber{"*cccccddddd*"})
}

func BenchmarkEndGlobNotMatching_ryanuber(b *testing.B) {
	benchmarkGlobNotMatching(b, &ryanuberGlobber{"aaaaabbbbbccccc*"})
}
