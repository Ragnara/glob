package benchmarks

import (
	"bitbucket.org/ragnara/glob"
	"testing"
)

type recompilingGlobber struct {
	pattern string
}

func (r *recompilingGlobber) Glob(s string) bool {
	return glob.Glob(s, r.pattern)
}

func BenchmarkStartGlobMatching_own_recompiling(b *testing.B) {
	benchmarkGlobMatching(b, &recompilingGlobber{"*dddddeeeeefffff"})
}

func BenchmarkMiddleGlobMatching_own_recompiling(b *testing.B) {
	benchmarkGlobMatching(b, &recompilingGlobber{"*cccccddddd*"})
}

func BenchmarkEndGlobMatching_own_recompiling(b *testing.B) {
	benchmarkGlobMatching(b, &recompilingGlobber{"aaaaabbbbbccccc*"})
}

func BenchmarkStartGlobNotMatching_own_recompiling(b *testing.B) {
	benchmarkGlobNotMatching(b, &recompilingGlobber{"*dddddeeeeefffff"})
}

func BenchmarkMiddleGlobNotMatching_own_recompiling(b *testing.B) {
	benchmarkGlobNotMatching(b, &recompilingGlobber{"*cccccddddd*"})
}

func BenchmarkEndGlobNotMatching_own_recompiling(b *testing.B) {
	benchmarkGlobNotMatching(b, &recompilingGlobber{"aaaaabbbbbccccc*"})
}
