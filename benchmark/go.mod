module bitbucket.org/ragnara/glob/benchmark

go 1.13

require github.com/ryanuber/go-glob v1.0.0

replace bitbucket.org/ragnara/glob => ../

require bitbucket.org/ragnara/glob v0.0.0
