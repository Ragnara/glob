package glob_test

import (
	"bitbucket.org/ragnara/glob"
	"fmt"
	"testing"
)

func ExampleGlobber_Glob() {
	testdata := []string{
		"cat",
		"dog",
		"bird",
		"fish",
		"lizard",
		"hedgehog",
		"fox",
	}

	endingWithDGlobber := glob.New("*d", glob.Default)

	for _, s := range testdata {
		if endingWithDGlobber.Glob(s) {
			fmt.Println(s, "ends with a 'd'")
		}
	}

	//Output:
	//bird ends with a 'd'
	//lizard ends with a 'd'
}

func ExampleGlob() {
	testdata := []string{
		"cat",
		"dog",
		"bird",
		"fish",
		"lizard",
		"hedgehog",
		"fox",
	}

	for _, s := range testdata {
		if glob.Glob(s, "*i*") {
			fmt.Println(s, "contains an 'i'")
		}
	}

	//Output:
	//bird contains an 'i'
	//fish contains an 'i'
	//lizard contains an 'i'
}

func TestGlob(t *testing.T) {
	tests := []struct {
		pattern string
		s       string
		desc    glob.PatternDescription
	}{
		{"*d*", "dabcdef", glob.Default},
		{"*d", "dabcdefd", glob.Default},
	}

	for i, test := range tests {
		g := glob.New(test.pattern, test.desc)
		if !g.Glob(test.s) {
			t.Errorf("%v: '%v' did not match pattern '%v'", i, test.s, test.pattern)
		}
	}
}
