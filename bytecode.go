package glob

import (
	"bytes"
	"strings"
	"unicode/utf8"
)

const (
	endOfString byte = 255 - iota
	skipAhead
	maxRun
)

func execute(code []byte, s string) bool {
	result := true
	shouldSkipAhead := false

	sb := []byte(s)
	i := 0

	var needle []byte

	for len(code) > 0 && result {
		cur := code[0]
		code = code[1:]

		switch cur {
		case endOfString:
			if i != len(sb) && !bytes.HasSuffix(sb, needle) {
				result = false
			}
		case skipAhead:
			shouldSkipAhead = true
		default:
			needle = code[:cur]
			code = code[cur:]

			found := -1
			if shouldSkipAhead {
				found = bytes.Index(sb[i:], needle)
			} else if bytes.HasPrefix(sb[i:], needle) {
				found = 0
			}
			if found == -1 {
				result = false
			} else {
				i += found + int(cur)
			}

			shouldSkipAhead = false
		}
	}

	return result
}

func compile(pattern string, description PatternDescription, buffer []byte) []byte {
	result := buffer

	b := []byte(pattern)
	endsWithWildcard := strings.HasSuffix(pattern, string(description.Wildcard))

	if utf8.RuneLen(description.Wildcard) == 1 {
		compileSingleByteWildcard(&result, b, byte(description.Wildcard))
	} else {
		compileUnicodeWildcard(&result, b, description)
	}

	if !endsWithWildcard {
		result = append(result, endOfString)
	}

	return result
}

func compileSingleByteWildcard(result *[]byte, b []byte, wildcard byte) {
	for len(b) > 0 {
		cur := b[0]
		morePending := len(b) > 1
		if cur == wildcard && morePending {
			*result = append(*result, skipAhead)
			b = b[1:]
		} else if cur == wildcard {
			break
		} else {
			copyNonWildcardWithSingleByteWildcard(result, &b, wildcard)
		}
	}
}

func copyNonWildcardWithSingleByteWildcard(result *[]byte, patternBytes *[]byte, wildcard byte) {
	currentBytes := (*patternBytes)[:]
	var collected []byte
	for len(*patternBytes) > 0 {
		cur := (*patternBytes)[0]
		newSize := 1 + len(collected)

		if cur == wildcard || newSize > int(maxRun) {
			break
		}

		collected = currentBytes[:newSize]
		*patternBytes = currentBytes[newSize:]
	}

	*result = append(*result, byte(len(collected)))
	*result = append(*result, collected...)
}

func compileUnicodeWildcard(result *[]byte, b []byte, description PatternDescription) {
	for len(b) > 0 {
		cur, size := utf8.DecodeRune(b)
		morePending := len(b) > size
		if cur == description.Wildcard && morePending {
			*result = append(*result, skipAhead)
			b = b[size:]
		} else if cur == description.Wildcard {
			break
		} else {
			copyNonWildcardWithUnicodeWildcard(result, &b, description)
		}
	}
}

func copyNonWildcardWithUnicodeWildcard(result *[]byte, patternBytes *[]byte, description PatternDescription) {
	currentBytes := (*patternBytes)[:]
	var collected []byte
	for len(*patternBytes) > 0 {
		cur, size := utf8.DecodeRune(*patternBytes)
		newSize := size + len(collected)

		if cur == description.Wildcard || newSize > int(maxRun) {
			break
		}

		collected = currentBytes[:newSize]
		*patternBytes = currentBytes[newSize:]
	}

	*result = append(*result, byte(len(collected)))
	*result = append(*result, collected...)
}
