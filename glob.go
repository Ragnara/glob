//Package glob contains an implementation for a globbing algorithm that uses a intermediate
//bytecode representation to allow efficient reuse of single pattern. The implementation
//supports any rune as a wildcard.
package glob

import (
	"sync"
)

//PatternDescription describes the matching semantics of the pattern language.
type PatternDescription struct {
	//Wildcard is the rune that is supposed to match 0..n runes.
	Wildcard rune
}

//Default is a PatternDescription with wildcard '*'
var Default = PatternDescription{Wildcard: '*'}

//SQLLike is a PatternDescription with wildcard '%'
var SQLLike = PatternDescription{Wildcard: '%'}

//Globber matches strings using a precompiled pattern.
type Globber struct {
	bc []byte
}

//New creates a new globber by compiling a pattern according to the given
//PatternDescription.
func New(pattern string, description PatternDescription) Globber {
	var g Globber
	g.bc = compile(pattern, description, make([]byte, 0, len(pattern)+2))
	return g
}

//Glob returns whether the globbers pattern matches the given string.
func (g Globber) Glob(s string) bool {
	return execute(g.bc, s)
}

//Glob is an auxiliary function that returns whether the given pattern as interpreted
//by the Default PatternDescription matches the given string.
//
//For efficiency reasons, it is better to build a globber and reuse it than to call
//this function multiple times if the pattern doesn't change. Each call of this function
//recompiles the pattern to a new globber.
func Glob(s string, pattern string) bool {
	buf := bufPool.Get().(*[]byte)
	g := Globber{compile(pattern, Default, *buf)}

	result := g.Glob(s)

	(*buf) = (*buf)[0:0]
	bufPool.Put(buf)
	return result
}

var bufPool = sync.Pool{
	New: func() interface{} {
		buf := make([]byte, 0, 64)
		return &buf
	},
}
