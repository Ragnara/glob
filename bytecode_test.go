package glob

import (
	"reflect"
	"testing"
)

func TestExecute(t *testing.T) {
	tests := []struct {
		code     []byte
		s        string
		expected bool
	}{
		{[]byte{}, "", true},
		{[]byte{}, "abc", true},
		{[]byte{3, 97, 98, 99, 255}, "abc", true},
		{[]byte{254, 1, 97}, "abc", true},
		{[]byte{3, 97, 98, 99, 255}, "abcd", false},
		{[]byte{3, 97, 98, 99}, "abc", true},
		{[]byte{3, 97, 98, 99, 255}, "abd", false},
		{[]byte{3, 97, 98, 99, 255}, "ababc", false},
		{[]byte{254, 3, 97, 98, 99, 255}, "ababc", true},
		{[]byte{254, 3, 97, 98, 99, 255}, "abcabc", true},
		{[]byte{254, 3, 97, 98, 99, 2, 65, 66, 255}, "ababcdAB", false},
		{[]byte{254, 3, 97, 98, 99, 254, 2, 65, 66, 255}, "ababcdAB", true},
		{[]byte{254, 3, 97, 98, 99, 2, 65, 66, 255}, "ababcAB", true},
		{[]byte{254, 3, 97, 194, 181, 254, 1, 99, 255}, "XaµXYc", true},
		{[]byte{254, 3, 97, 194, 181, 254, 1, 99, 255}, "XaµXY", false},
		{[]byte{254, 3, 97, 194, 181, 254, 1, 99, 255}, "XaXYc", false},
	}

	for i, test := range tests {
		result := execute(test.code, test.s)
		if result != test.expected {
			t.Errorf("%v, bc %v, s '%v': Expected %v, got %v", i, test.code, test.s, test.expected, result)
		}
	}
}

func TestCompile(t *testing.T) {
	tests := []struct {
		pattern  string
		expected []byte
	}{
		{"*", []byte{}},
		{"abc*", []byte{3, 97, 98, 99}},
		{"*d*", []byte{254, 1, 100}},
		{"*abc*", []byte{254, 3, 97, 98, 99}},
		{"*ab*c*", []byte{254, 2, 97, 98, 254, 1, 99}},
		{"*ab*c", []byte{254, 2, 97, 98, 254, 1, 99, 255}},
		{"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
			"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			[]byte{253, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97,
				97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 97, 2, 97, 97, 255}},
	}

	for i, test := range tests {
		result := compile(test.pattern, Default, make([]byte, 0, 16))
		if !reflect.DeepEqual(result, test.expected) {
			t.Errorf("%v, '%v': Expected %v, but got %v", i, test.pattern, test.expected, result)
		}
	}
}

func TestCompileSQLLike(t *testing.T) {
	tests := []struct {
		pattern  string
		expected []byte
	}{
		{"%", []byte{}},
		{"abc%", []byte{3, 97, 98, 99}},
		{"%abc", []byte{254, 3, 97, 98, 99, 255}},
		{"%abc%", []byte{254, 3, 97, 98, 99}},
		{"abc*", []byte{4, 97, 98, 99, 42, 255}},
	}

	for i, test := range tests {
		result := compile(test.pattern, SQLLike, make([]byte, 0, 16))
		if !reflect.DeepEqual(result, test.expected) {
			t.Errorf("%v, '%v': Expected %v, but got %v", i, test.pattern, test.expected, result)
		}
	}
}

func TestCompileUnicodeWildcard(t *testing.T) {
	tests := []struct {
		pattern  string
		expected []byte
	}{
		{"€", []byte{}},
		{"abc€", []byte{3, 97, 98, 99}},
		{"€abc€", []byte{254, 3, 97, 98, 99}},
		{"€ab€c€", []byte{254, 2, 97, 98, 254, 1, 99}},
		{"€ab€c", []byte{254, 2, 97, 98, 254, 1, 99, 255}},
		{"aµc€", []byte{4, 97, 194, 181, 99}},
		{"€aµc€", []byte{254, 4, 97, 194, 181, 99}},
		{"€aµ€c€", []byte{254, 3, 97, 194, 181, 254, 1, 99}},
		{"€aµ€c", []byte{254, 3, 97, 194, 181, 254, 1, 99, 255}},
		{"µµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµµ",
			[]byte{252, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181,
				194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 194, 181, 4, 194, 181, 194, 181, 255}},
	}

	for i, test := range tests {
		result := compile(test.pattern, PatternDescription{Wildcard: '€'}, make([]byte, 0, 16))
		if !reflect.DeepEqual(result, test.expected) {
			t.Errorf("%v, '%v': Expected %v, but got %v", i, test.pattern, test.expected, result)
		}
	}
}
