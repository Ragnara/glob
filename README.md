[![GoDoc](https://godoc.org/bitbucket.org/Ragnara/glob?status.svg)](https://godoc.org/bitbucket.org/Ragnara/glob) [![Go Report Card](https://goreportcard.com/badge/bitbucket.org/Ragnara/glob)](https://goreportcard.com/report/bitbucket.org/Ragnara/glob)

# glob - Fast and flexible globbing for Go

This module contains a globbing implementation that can be used with any rune as a wildcard character.

For documentation please read the godoc.
